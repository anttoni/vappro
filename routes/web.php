<?php

use App\Helpers\Checkpoint;
use App\Models\Achievement;
use App\Models\Attendee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function (Request $request) {

    $data = [
        'attendee' => null,
        'checkpoints' => Checkpoint::list(),
    ];

    if ($attendeeHash = $request->cookie('attendee')) {
        $data['attendee'] = Attendee::where('password', $attendeeHash)->first();
    }

    return view('main', $data);
});

Route::get('/password', function (Request $request) {

    $password = $request->get('q');

    if (RateLimiter::tooManyAttempts('forgot-password', 20)) {
        return 'Too many attempts!';
    }

    RateLimiter::hit('forgot-password');

    $redirect = '/';

    ray($request->get('next'));

    if ($request->has('next')) {
        $redirect = $request->get('next');
    }

    if ($attendee = Attendee::where('password', $password)->first()) {
        return redirect($redirect)->withCookie('attendee', $attendee->password);
    }

    return redirect($redirect);
});

Route::get('some/{attendee:name?}', function (?Attendee $attendee) {
    // Get achievements newest first
    if (empty($attendee)) {
        // Get achievements for attendee
        $achievements = $attendee->achievements()->orderBy('created_at', 'desc')->get();
    } else {
        $achievements = Achievement::orderBy('created_at', 'desc')->get();
    }

    $data = ['achievements' => $achievements];

    if ($attendeeHash = request()->cookie('attendee')) {
        $data['user'] = Attendee::where('password', $attendeeHash)->first();
    }

    return view('some', $data);
});
