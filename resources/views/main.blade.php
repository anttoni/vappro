<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
   
        <title>vELAATAN appro</title>

        <!-- Fonts -->
        @livewireStyles()
        @livewireScripts

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="antialiased p-2">
        <a href="/"><img src="https://maitolaiturimuseo.net/img/postikortti.png"></a>
        <h1 class="text-3xl my-3">Velaatan appro</h1>
        <h2 class="text-2xl my-2">9.8.2023-13.8.2023</h2>
        <p>Velaatan appro on elokuussa järjestettävä etäappro. Voit suorittaa sen jos olet tulossa Maukan mökille.</p>
        <hr class="my-6 border-gray-400">
        @if(  $attendee )
            <p class="text-xl">Hei {{$attendee->name}}!</p>
                @if( !empty(\App\Helpers\Checkpoint::list($attendee)))
                <a class="text-blue-700 underline" href="https://www.google.com/maps/dir/Sale+Ter%C3%A4lahti,+Ter%C3%A4lahdentie+1090,+34260+Ter%C3%A4lahti/Velaatantie+445,+34270+Tampere/Velaatan+maitolaiturimuseo,+Velaatantie,+Velaatta/Muroleen+kanava,+Ruovesi/Velaatantie+1234,+34270+Tampere/@61.781153,23.7928316,11z/data=!3m1!4b1!4m32!4m31!1m5!1m1!1s0x468f3b1e46a730a1:0x4c940229b3d87d30!2m2!1d23.9001597!2d61.7070055!1m5!1m1!1s0x468f3a49121c9d2d:0x8168b3096e698d83!2m2!1d23.9201251!2d61.745362!1m5!1m1!1s0x468f3a336cb83db1:0x3ee2b8ce678036ed!2m2!1d23.9354982!2d61.7517723!1m5!1m1!1s0x468f37c1d85bda73:0xffd21ea3666c1389!2m2!1d23.9067283!2d61.8555197!1m5!1m1!1s0x468f3913df64c6b5:0xec8c9b04228ba836!2m2!1d24.0155299!2d61.7883795!3e0?entry=ttu">
                Suositeltu suoritusjärjestys ennen mökille saapumista.</a>
                <livewire:record-achievement-form :attendee="$attendee"/>
                @else 
                <p class="text-xl">Appro pelattu läpi, ota merkki!</p>
                <img src="{{url('/virallinen_velaatan_appron_siili.png')}}" class="w-96">
                @endif
            <h2 class="text-2xl my-2">Kohteet</h2>
            @foreach( $checkpoints as $index => $checkpoint )
                <livewire:checkpoint :attendee="$attendee" :checkpoint="$checkpoint"/>
            @endforeach
        @else 
            <livewire:register-attendee-form />
            <hr class="my-6 border-gray-400">
            <h2 class="text-2xl my-2 mt-4">Kohteet</h2>
            @foreach( $checkpoints as $checkpoint )
                @include('partials.checkpoint')
            @endforeach
        @endif
    </body>
</html>
