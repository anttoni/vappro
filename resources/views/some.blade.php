<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
   
        <title>vELAATAN appro</title>

        <!-- Fonts -->
        @livewireStyles()
        @livewireScripts

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="antialiased p-2">
        <a href="/"><img src="https://maitolaiturimuseo.net/img/postikortti.png"></a>
        <h1 class="text-3xl my-3">Velaatan appro</h1>
        <h2 class="text-2xl my-2">9.8.2023-13.8.2023</h2>
        <p>Velaatan appro on elokuussa järjestettävä etäappro. Voit suorittaa sen jos olet tulossa Maukan mökille.</p>
        <hr class="my-6 border-gray-400">
        <h2 class="text-2xl">Suorituksia</h2>
        @if( empty($user) ) 
            <span class="my-2 text-md">Kirjaudu sisään kommentoidaksesi<span> <form class="inline-block" method="get" action="/password">
                <input class="inline-block" type="text" name="q" id="q">
                <input type="hidden" name="next" id="next" value="/some">
                <button class="inline-block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 my-2 focus:outline-none"
                type="submit">ju</button>
            </form>
           
        @endif
        @foreach( $achievements as $achievement )
            <div class="border my-2 p-3">
                <p class="text-xl">{{App\Models\Attendee::find($achievement->uploaded_by)->name}} - {{ Carbon\Carbon::create($achievement->created_at)->addHours(3)}}</p>
                <p class="text-lg italic">@ {{App\Helpers\Checkpoint::name($achievement->checkpoint_id) }}</p>
                <img src="/storage/achievements/{{$achievement->image_hash}}" class="max-w-xs">
                <p class="text-sm">Suorittajat: {{ implode( ', ', $achievement->attendees->pluck('name')->all() )}} </p>
                <p class="text-md my-2">"{{$achievement->comment}}"</p>
                <livewire:comments :achievement="$achievement" :user="$user ?? null"/>
            </div>
        @endforeach
    </body>
</html>