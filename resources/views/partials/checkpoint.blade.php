<div class="border p-4 my-2">
    <h3 class="text-xl
    @if( $achievement ?? null ) 
    text-green-700
    @endif
    ">{{$checkpoint['name'] }}
    @if( $achievement ?? null ) 
    (suoritettu ✅)
    @endif
    </h3>

    <p class="italic">{{$checkpoint['description'] }}</p>
    @if( ! ($achievement ?? null) ) 
    <a class="inline-block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 my-2 focus:outline-none" 
    href="{{$checkpoint['google_maps_url'] }}" target="_blank">Sijainti  🗺 ↗️</a>
    @endif
    @if( $achievement ?? null ) 
    <div class="py-3">
        Suoritus:
        <img class="w-24" src="{{ asset('storage/achievements/' . $achievement->image_hash) }}">
        <a class="text-blue-700 underline" href="{{ asset('storage/achievements/' . $achievement->image_hash) }}" target="_blank">
        Lataa ⬇️
        </a>
    </div>
    @endif
</div>
