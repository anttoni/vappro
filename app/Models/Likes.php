<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $attendee_id
 * @property int $likeable_id
 * @property int $likeable_type
 */
class Likes extends Model
{
    use HasFactory;

    protected $fillable = [
        'attendee_id',
        'likeable_id',
        'likeable_type',
    ];

    public function likeable()
    {
        return $this->morphTo();
    }

    public function attendee()
    {
        return $this->belongsTo(Attendee::class);
    }
}
