<label
for="formFile"
class="mb-2 mt-4 inline-block text-neutral-700"
>2. Valitse suorituspaikka</label>
<div class="max-w-xs full-w">
<select wire:model.defer="checkpoint" class="w-full">
    <option value="">Valitse suorituspaikka</option>
    @foreach( $checkpoints as $index => $checkpoint )
        <option value="{{$index}}">{{$checkpoint['name']}}</option>
    @endforeach
</select>
@error('checkpoint') <span class="error">{{ $message }}</span> @enderror
</div>