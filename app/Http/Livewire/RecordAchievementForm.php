<?php

namespace App\Http\Livewire;

use App\Helpers\Checkpoint;
use App\Models\Achievement;
use App\Models\Attendee;
use Livewire\Component;
use Livewire\WithFileUploads;

class RecordAchievementForm extends Component
{
    use WithFileUploads;

    public $checkpoints;

    public $attendee;

    public $photo;

    public $comment;

    public $participants;

    public $attendees;

    public $checkpoint;

    public $showForm = false;

    public $successMessage;

    public function render()
    {
        return view('livewire.record-achievement-form');
    }

    public function mount(Attendee $attendee)
    {
        $this->attendee = $attendee;
        $this->checkpoints = Checkpoint::list($attendee);
        $this->attendees = Attendee::all()->map(
            function ($attendee) {
                return [
                    'id' => $attendee->id,
                    'name' => $attendee->name,
                ];
            }
        );
    }

    public function save()
    {

        ray($this->photo);

        $this->validate([
            'photo' => 'image|max:10000', // 2MB Max
            'comment' => 'required|string',
            'participants' => 'required|array|min:1',
            'checkpoint' => 'required|integer|min:0|max:4',
        ]);

        $extension = $this->photo->getClientOriginalExtension();
        $imagehash = uniqid();
        $this->photo->storePubliclyAs('achievements', "$imagehash.$extension", 'public');

        $achievement = Achievement::create([
            'checkpoint_id' => $this->checkpoint,
            'image_hash' => "$imagehash.$extension",
            'comment' => $this->comment,
            'uploaded_by' => $this->attendee->id,
        ]);

        //add attendees to achievement
        $this->participants[] = $this->attendee->id;

        $achievement->attendees()->attach(array_unique($this->participants));
        $this->successMessage = 'Suoritus lisätty!';
        $this->showForm = false;
    }
}
