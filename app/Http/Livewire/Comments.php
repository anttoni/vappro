<?php

namespace App\Http\Livewire;

use App\Models\Comment;
use App\Models\Likes;
use Livewire\Component;

class Comments extends Component
{
    public $achievement;

    public $user;

    public $comments = [];

    public $newComment = '';

    public function render()
    {
        return view('livewire.comments');
    }

    public function mount($achievement, $user = null)
    {
        $this->achievement = $achievement;
        $this->user = $user;
        $this->comments = $achievement->comments()->with('likes')->get();
    }

    public function comment()
    {
        $this->validate([
            'newComment' => 'required|min:1',
        ]);

        $comment = Comment::create([
            'attendee_id' => $this->user->id,
            'achievement_id' => $this->achievement->id,
            'comment' => $this->newComment,
        ]);

        $this->newComment = '';

        $this->comments->push($comment);
    }

    public function likeComment($id)
    {
        $comment = Comment::find($id);

        $comment->likes()->create([
            'attendee_id' => $this->user->id,
        ]);

        $this->comments = $this->achievement->comments()->with('likes')->get();
    }

    public function likeAchievement()
    {
        $this->achievement->likes()->create([
            'attendee_id' => $this->user->id,
        ]);

        $this->achievement->likes = Likes::where('likeable_id', $this->achievement->id)->get();
    }
}
