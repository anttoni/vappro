<div class="border my-3 p-4">
    <h2 class="text-2xl my-3">Joinaa!</h2>
    <input class="block" wire:model.lazy="username">
    <button class="inline-block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 my-2 focus:outline-none"
     wire:click="register">Rekisteröidy</button>
    @if( $errors->any() )
        @foreach( $errors->all() as $error )
        <p>{{$error}}</p>
        @endforeach
    @endif

    @if( $attendee )
        <h2>Tervetuloa! {{$attendee->name}}</h2>
        <p>Tässä salasanasi, ota se talteen!</p>
        <input type="text" value="{{$attendee->password}}" disabled>
        <button onclick="navigator.clipboard.writeText('{{$attendee->password}}')">Kopioi</button>
        <p>Salasanaa tarvitaan, jos juuri asetettu keksi hukkuu tai haluat päästä käsiksi käyttäjään eri selaimesta.</p>
        <a class="inline-block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 my-2 focus:outline-none" href="/">Ju. Sit vaan!</a>
    @endif

    <p class="my-2 text-xl">Minulla on salasana</p>
    <form method="get" action="/password">
        <input type="text" name="q" id="q">
        <button class="inline-block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 my-2 focus:outline-none"
        type="submit">Kirjaudu</button>
    </form>

</div>