<div wire:key="comments-of-{{$achievement->id}}">
    @if( $user && !in_array(  $user->id, $achievement->likes->pluck('attendee_id')->all()) )
    <button class="block text-white bg-blue-500 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-3 py-1.5 mr-2 mb-2 focus:outline-none" 
        wire:click="likeAchievement">Tykkää</button>
    @endif
     @foreach( $achievement->likes as $like )
            👍  <span class="text-xs text-gray-500 mr-1">{{$like->attendee->name}} </span>
    @endforeach

    <p class="text-xl mt-2">Kommentit</p>
    @foreach ($comments as $comment) 
        <div class="border py-3 px-1" wire:key="comment-{{$comment->id}}">
            <p class="text-md"><span class="text-blue-800 font-bold">{{$comment->attendee->name}}</span> {{ Carbon\Carbon::create($comment->created_at)->addHours(3)}} </p>
            <p>{{$comment->comment}}</p>
            @if( $user && !in_array(  $user->id, $comment->likes->pluck('attendee_id')->all()) )
            <button class="text-white bg-blue-500 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-xs px-1 py-1 mr-2 mb-2 focus:outline-none"
                wire:click="likeComment({{$comment->id}})">Tykkää</button>
            @endif
            
            @foreach( $comment->likes as $like )
               👍  <span class="text-xs text-gray-500 mr-1">{{$like->attendee->name}} </span>
            @endforeach
        </div>
    
    @endforeach
    @if( $user )
    <form wire:submit.prevent="comment">
        <textarea class="block my-3 py-2" type="text" wire:model.defer="newComment"></textarea>
        <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 focus:outline-none">Lisää kommentti</button>
    </form>
    @endif 
</div>


