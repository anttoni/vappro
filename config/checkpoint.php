<?php

return [
    'locations' => [
        [
            'id' => 0,
            'name' => 'Maitolaiturimuseo',
            'description' => 'Euroopan pienin museo!',
            'google_maps_url' => 'https://goo.gl/maps/JmvxCHwChM2KoJEc7',
        ],
        [
            'id' => 1,
            'name' => 'Velaatan Baari 🪦',
            'description' => 'Lepää rauhassa',
            'google_maps_url' => 'https://goo.gl/maps/ptAMtqwr4B9yxQVN8',
        ],
        [
            'id' => 2,
            'name' => 'Sale Terälahti',
            'description' => 'Evästä matkaan',
            'google_maps_url' => 'https://goo.gl/maps/bwRqe2KLtKyc6XaJ7',
        ],
        [
            'id' => 3,
            'name' => 'Muroleen kanava',
            'description' => 'Pikkukierto, vai soutaen viikonlopun aikana?',
            'google_maps_url' => 'https://goo.gl/maps/3qetv7eeDP66ynZk6',
        ],
        [
            'id' => 4,
            'name' => 'Mökki',
            'description' => 'Viimein perillä!',
            'google_maps_url' => 'https://www.ruovesi.fi/',
        ],
    ],
];
