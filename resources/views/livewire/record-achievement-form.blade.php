<div>
    <p>Ohjeet kohteiden suorittamiseen:</p>
    <ul class="m-2 p-3 list-decimal">
        <li>Mene kohteeseen</li>
        <li>Ota (ryhmä)selfie, jossa näkyy 
            <ul class="p-1 ml-2 list-disc">
                <li>kohde</li>
                <li>sinä ja mahdolliset muut suorittajat nauttimassa juomia</li>
            </ul>
         <li>Lataa kuva ja tiedot Lisää suoritus-napista.</li>
    </ul>

    @if( $showForm ) 
<form class="border my-2 p-3" wire:submit.prevent="save">
    @include('partials.achievement-photo')
    @include('partials.checkpoint-selector')
    @include('partials.participant-selector')
    @include('partials.comment-field')
    <button class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 my-2 focus:outline-none">Ju!</button>
</form>
@else
<div x-data="{ show: true }" x-show="show" x-init="setTimeout(() => show = false, 3000)">
<span class="color-green">
    {{ $successMessage }}
</span>
</div>
    <button class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 my-2 focus:outline-none" 
    wire:click="$toggle('showForm')">Lisää suoritus</button>
@endif
</div>
</div>