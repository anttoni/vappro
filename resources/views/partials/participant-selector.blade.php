
<label
for="formFile"
class="mb-2 mt-4 inline-block text-neutral-700">3. Ketä kuvassa?</label>
<div class="w-full">
        <select wire:model.defer="participants" multiple>
            @foreach( $attendees as $attendee )
                <option value="{{$attendee['id']}}">{{$attendee['name']}}</option>
            @endforeach
        </select>
    </div>
    @error('participants') <span class="error">{{ $message }}</span> @enderror
