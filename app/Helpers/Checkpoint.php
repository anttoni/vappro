<?php

namespace App\Helpers;

use App\Models\Attendee;

class Checkpoint
{
    public static function list(Attendee $attendee = null)
    {
        $locations = config('checkpoint.locations');

        if ($attendee) {
            $achieved = $attendee->achievements()->pluck('checkpoint_id')->toArray();
            //remove achieved
            $locations = array_filter($locations, function ($location) use ($achieved) {
                return ! in_array($location['id'], $achieved);
            });
        }

        return $locations;
    }

    public static function name(int $id)
    {
        return config('checkpoint.locations')[$id]['name'];
    }

    public static function toSelectorJson()
    {
        return json_encode(collect(Checkpoint::list())->map(function ($checkpoint, $index) {
            return [
                'id' => $index,
                'name' => $checkpoint['name'],
                'disabled' => false,
            ];
        })->all());
    }
}
