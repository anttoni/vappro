<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Achievements can be earned by attendees by uploading a image.
 *
 * @property int $id
 * @property string $checkpoint_id
 * @property string $image_hash
 * @property string $comment
 * @property int $uploaded_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class Achievement extends Model
{
    use HasFactory;

    protected $fillable = [
        'checkpoint_id',
        'image_hash',
        'comment',
        'uploaded_by',
    ];

    /**
     * Many-to-many relationship with attendees
     */
    public function attendees()
    {
        return $this->belongsToMany(Attendee::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likes()
    {
        return $this->morphMany(Likes::class, 'likeable');
    }
}
