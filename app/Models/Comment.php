<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $attendee_id
 * @property int $achievement_id
 * @property string $comment
 */
class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'attendee_id',
        'achievement_id',
        'comment',
    ];

    public function attendee()
    {
        return $this->belongsTo(Attendee::class);
    }

    public function achievement()
    {
        return $this->belongsTo(Achievement::class);
    }

    public function likes()
    {
        return $this->morphMany(Likes::class, 'likeable');
    }
}
