<?php

namespace App\Http\Livewire;

use App\Models\Achievement;
use App\Models\Attendee;
use Livewire\Component;

class Checkpoint extends Component
{
    public ?Attendee $attendee;

    public ?Achievement $achievement;

    public $checkpoint;

    public function render()
    {
        return view('livewire.checkpoint');
    }

    public function mount(?Attendee $attendee, array $checkpoint)
    {
        $this->attendee = $attendee;
        $this->achievement = $attendee->achievements()->where('checkpoint_id', $checkpoint['id'])->first() ?? null;
        $this->checkpoint = $checkpoint;
    }
}
