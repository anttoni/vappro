<?php

namespace App\Http\Livewire;

use App\Models\Attendee;
use Illuminate\Support\Facades\Cookie;
use Livewire\Component;

class RegisterAttendeeForm extends Component
{
    public $username;

    public Attendee $attendee;

    public function render()
    {
        return view('livewire.register-attendee-form');
    }

    public function register()
    {

        $this->validate([
            'username' => 'required|unique:attendees,name',
        ], [
            'username.required' => 'Tarkemmin, tarkemmin!',
            'username.unique' => 'Ton niminen jarmo löytyy jo!',
        ]);

        $attendee = new Attendee([
            'name' => $this->username,
            'password' => Attendee::generatePassword(),
        ]);

        $this->attendee = $attendee;

        Cookie::queue('attendee', $attendee->password);

        $attendee->save();
    }
}
