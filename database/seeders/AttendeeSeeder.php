<?php

namespace Database\Seeders;

use App\Models\Attendee;
use Illuminate\Database\Seeder;

class AttendeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Attendee::factory()
            ->count(10)
            ->hasAchievements(5, function (array $attributes, Attendee $attendee) {
                return ['uploaded_by' => $attendee->id];
            })
            ->create();
    }
}
