<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Achievement>
 */
class AchievementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            // Number between 0 and 4
            'checkpoint_id' => $this->faker->numberBetween(0, 4),
            'image_hash' => 'kitten.jpg',
            'comment' => $this->faker->sentence,
            'uploaded_by' => 1,
        ];
    }

    /**
     * Indicate that the achievement was uploaded by a specific attendee.
     */
    public function uploadedBy(int $attendeeId): Factory
    {
        return $this->state([
            'uploaded_by' => $attendeeId,
        ]);
    }
}
