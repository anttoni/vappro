<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Attendees are the people who attend vappro and upload images.
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class Attendee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'password',
    ];

    /**
     * Many-to-many relationship with achievements
     */
    public function achievements()
    {
        return $this->belongsToMany(Achievement::class);
    }

    public static function generatePassword()
    {
        // Generate all caps alphanumeric password with 5 characters.
        return strtoupper(substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyz'), 0, 5));
    }

    public static function toSelectorJson()
    {

        $attendees = Attendee::all();

        return json_encode($attendees->map(function ($attendee) {
            return [
                'value' => $attendee->id,
                'label' => $attendee->name,
            ];
        })->all());
    }
}
