    <label
    for="formFile"
    class="mb-2 mt-4 inline-block text-neutral-700"
    >4. Kommentoi suoritusta ja/tai kohdetta.</label>

    <textarea class="block" wire:model.defer="comment"></textarea>
    @error('comment') <span class="error">{{ $message }}</span> @enderror